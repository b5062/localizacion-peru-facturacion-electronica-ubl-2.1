# -*- coding: utf-8 -*-
from odoo import models, fields

class BoProductBrand(models.Model):
    _name = 'bo.product.brand'
    _description = "Marca de Producto"

    name = fields.Char(string="Nombre",default="")
    #code = fields.Char(string="Código",default="")

    description = fields.Text(string="Descripción",default="")

    """
    _sql_constraints = [
        ('brand_code_unique','unique(code)','El código de la marca ya existe, este debe ser único !'),
    ]
    """
