from odoo import models, fields

class ProductTemplate(models.Model):
    _inherit='product.template'
    
    bo_brand_id = fields.Many2one('bo.product.brand', string="Marca")

class ProductProduct(models.Model):
    _inherit='product.product'
    
    bo_brand_id = fields.Many2one('bo.product.brand', string="Marca",related="product_tmpl_id.bo_brand_id")