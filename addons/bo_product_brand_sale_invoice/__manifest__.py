{
	'name': 'Marca de producto en ventas, compras y facturas',
	'version': "1.0.0",
	'author': 'Bigodoo - Christian',
	'depends':['base','bo_product_brand','gestionit_pe_fe'],
	'data':[
    	"reports/sale_report.xml",
        "reports/purchase_report.xml",
        "reports/invoice_report.xml",
	],
	'installable': True,
    'auto_install': False,
}