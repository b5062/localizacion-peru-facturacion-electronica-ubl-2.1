
class SunatLoginUsernameOrPasswordInvalid(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__("El nombre de usuario sunat o el password son inválidos")

class InvalidRUC(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__("El número de RUC es Inválido")

class InvalidTipoCPE(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__("Tipo de comprobante electrónico inválido")

class InvalidSerieCPE(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__("La serie del tipo de comprobante electrónico es inválido.")


class ServiceGRECDRException(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)